<?php $copyright = ot_get_option('copyright'); ?>

<footer id="footer">
    <div class="container-fluid">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <div class="text-center text-uppercase">
                <a href="javascript:;" data-action="scrollToTop">Back to Top</a>
            </div>
        </div>
        <div class="col-sm-4">

            <?php if(!empty($copyright)) { ?>

            <div class="copyright text-right">
                <p><?php echo $copyright; ?></p>
            </div>

            <?php } ?>

        </div>
    </div>
</footer>

<?php wp_footer(); ?>

<div id="dl-loader"><div class="loader-inner"><img src="<?php echo get_template_directory_uri(); ?>/images/DL2k_opt.gif"></div></div>

</body>
</html>
