var DigitalLight = function() {
    var productCarousel;
    var productCarouselThumbnails;
    var scrollToTopAction = '[data-action="scrollToTop"]';
    var closeModalWindowAction = '[data-action="closeModalWindow"]';
    var pageContentModalId = '#page-content';
    var productCarouselId = '#work-carousel';
    var productCarouselThumbnailsId = '#work-carousel-thumbnails';
    var toggleCategoryPostsAction = '[data-action="toggleCategoryPosts"]';
    var mobileCarouselId = '#mobile-carousel';
    var toggleMobileMenuAction = '[data-action="toggleMobileMenu"]';
    var closeAccordionTabAction = '[data-action="closeAccordionTab"]';
    var videoAutoPlayAction = '[data-action="videoAutoPlay"]';
    var contactMapId = '#contact-map';

    return {
        init: function() {
            DigitalLight.events();
            DigitalLight.loader(false);

            //console.log(location);

            //console.log(dl_config.blog_url);
            //console.log(location.href);

            if(dl_config.blog_url != location.href) {
                DigitalLight.getContent(location.href);
            }

            /*if(window.history.state == undefined) {
                DigitalLight.getContent('http://localhost/digital_light/portfolio/',true);
            }*/

            /*if($('#beauty').length) {
                console.log(1);
                $('#beauty').stick_in_parent({
                    fixed: true,
                    parent: '.list-articles-wrapper',
                    bottoming: true,
                    //inner_scrolling:false
                }).on("sticky_kit:stick", function(e) {
                    console.log("has stuck!", e.target);
                })
                    .on("sticky_kit:unstick", function(e) {
                        console.log("has unstuck!", e.target);
                    }).on('sticky_kit:bottom', function(e){
                        element.parent().addClass('bottomed');
                    })
                    .on('sticky_kit:unbottom', function(e){
                        element.parent().removeClass('bottomed')
                    });
            }*/

            //var stateObj = { foo: "bar" };
            //history.pushState(stateObj, "", "index.html");

            var currentState = window.history;

            //console.log(currentState);

            var savedPageStateURL = window.location.pathname;
            //window.history.replaceState( {}, null , url );
            /*if( 'pushState' in window.history && window.history.state )
                window.history.replaceState( {}, null , savedPageStateURL );

            $(window).bind('popstate', function() {

                console.log(location);

                $.ajax({url:location.pathname+'?rel=tab',success: function(data){
                    $('#content').html(data);
                }});
            });*/
        },

        events: function() {
            $(document).on('mouseover', '.article-item, .video-item', function() {
                var that = $(this);
                var info = $('.article-info',that);
                var infoHeight = info.innerHeight();

                if(info.length) {
                    info.css({
                        bottom: -infoHeight,
                        visibility: 'visible'
                    }).data('height',infoHeight).stop(true,true).animate({
                        bottom:0
                    },500)
                }
            });

            $(document).on('mouseleave', '.article-item, .video-item', function() {
                var that = $(this);
                var info = $('.article-info',that);
                var infoHeight = info.data('height');

                if(info.length) {
                    info.stop(true,true).animate({
                        bottom:-infoHeight
                    },500)
                }
            });

            $(scrollToTopAction).on('click', function() {
                $('body, html').animate({
                    scrollTop: 0
                }, 1000);
            });

            /*$(document).on('mouseover', '.article-item, .video-item', function() {
                var that = $(this);
                var info = $('.category-name',that);
                var infoHeight = info.innerHeight();

                if(info.length) {
                    info.css({
                        bottom: -infoHeight,
                        visibility: 'visible'
                    }).data('height',infoHeight).stop(true).animate({
                        bottom:0
                    },500)
                }
            });

            $(document).on('mouseleave', '.article-item, .video-item', function() {
                var that = $(this);
                var info = $('.category-name',that);
                var infoHeight = info.data('height');

                if(info.length) {
                    info.stop(true).animate({
                        bottom:-infoHeight
                    },500)
                }
            });*/

            $('.main-navigation a, .dynamic-load').on('click', function() {
                var that = $(this);
                var mainNav = that.closest('.main-navigation');
                var href = that.attr('href');
                var target = that.attr('target');
                var body = $('body');
                var dataHref = that.data('href');

                if(dataHref!=undefined) {
                    href = dataHref;
                    target = '';
                }

                if(target != '_blank') {
                    if(mainNav.length) {
                        $('li',mainNav).removeClass('current-menu-item');
                        that.parent().addClass('current-menu-item');
                    }

                    // Getting Content
                    DigitalLight.getContent(href, true);

                    //console.log(that.attr('href'))

                    body.removeClass('mobile-menu-opened');

                    return false;
                }
            });

            // function clickScrollOffset() {
            //     $('.dynamic-load').on('click', function() {
            //         var docScrollOffset = $(document).offset;
            //         console.log(docScrollOffset.top);
            //         $('#page-content').css({'top' : docScrollOffset.top});
            //     });
            // }
            // setTimeout(clickScrollOffset, 5000);

            $(closeModalWindowAction).on('click', function () {
                var modal = $(pageContentModalId);
                var body = $('body');

                if(modal.length) {
                    modal.slideUp(500);

                    history.pushState({
                        url: dl_config.blog_url
                    }, null, dl_config.blog_url);

                    body.removeClass('modal-opened');
					$('#page-content').removeClass('not-menu');
                    $('.main-navigation li').removeClass('current-menu-item');
                    $('a[href="' + dl_config.blog_url + '"]').parent().addClass('current-menu-item');

                    DigitalLight.vimeoPauseAll();
                }

                return false;
            });

            $(toggleMobileMenuAction).on('click', function () {
                var body = $('body');

                body.toggleClass('mobile-menu-opened');
            });

            $(closeAccordionTabAction).on('click', function () {
                var that = $(this);
                var column = that.closest('.article-column');

                $('.category-name',column).click();

                return false;
            });

            $(document).on('click','[data-action="customSlide"]', function () {
                var that = $(this);
                var direction = that.data('direction');

                if(direction == 'next') {
                    productCarousel.trigger("owl.next");
                } else {
                    productCarousel.trigger("owl.prev");
                }
            });

            $(document).on('mouseover', videoAutoPlayAction, function(e) {
                var box = $(this);
                var video_id = $(box).data('id');

                videojs(video_id).pause();

                e.preventDefault();
            });

            $(document).on('mouseleave', videoAutoPlayAction, function(e) {
                var box = $(this);
                var video_id = $(box).data('id');

                videojs(video_id).play();

                e.preventDefault();
            });

            $(document).on('click1', videoAutoPlayAction, function(e) {
                var box = $(this);
                var video_id = $(box).data('id');
                var vimeo_url = $(box).data('vimeo_url');

                if(video_id != undefined && (vimeo_url != '' && vimeo_url != undefined)) {
                    videojs(video_id).pause();

                    //vimeo_url = 'https://player.vimeo.com/video/57421694';
                    //console.log(DigitalLight.parseVimeoURL(vimeo_url))

                    $.fancybox({
                        href: DigitalLight.parseVimeoURL(vimeo_url),
                        type: 'iframe',
                        //fitToView: false,
                        padding: 5,
                        helpers: {
                            media: {}
                        },
                        beforeLoad: function() {
                            DigitalLight.loader(true);
                        },
                        afterLoad: function() {
                            DigitalLight.loader(false);
                        }
                    });
                } else {
                    alert("Error");
                }

                e.preventDefault();
            });
        },

        parseVimeoURL: function(url){
            var vimeoRegex = /(?:vimeo)\.com.*(?:videos|video|channels|)\/([\d]+)/i;
            var parsed = url.match(vimeoRegex);

            return "//player.vimeo.com/video/" + parsed[1];
        },

        calcCategoryTitles: function() {
            var catBox = $('.article-column .category-name');

            if(catBox.length) {
                var $scrollTop = $(window).scrollTop();
                var $footer = $('#footer');
                var bottomOffset = 0;
                var header = $('#header');

                if($footer.length) {
                    var footerHeight = $footer.innerHeight();

                    if($scrollTop + $(window).height() + footerHeight >= $(document).height()) {
                        bottomOffset = footerHeight;
                    }
                }

                catBox.each(function(i,e) {
                    var catColumn = $(e).closest('.article-column');
                    var catColumnWidth = catColumn.width();

                    $(e).width(catColumnWidth);

                    /*if(bottomOffset != 0) {

                        $(e).css({
                            top:header.innerHeight(),
                            bottom: 'auto'
                        });
                    } else {
                        $(e).css({
                            position: 'fixed',
                            top:'auto',
                            bottom: 0
                        });
                    }*/
                });
            }
        },

        getContent: function(url, addEntry) {
            if(url==undefined) {
                alert('URL not determined');
                return false;
            }

            // DigitalLight.loader(true);

            var body = $('body');
            var args = {action: 'get_content'};

            $.extend(args, {
                url: url
            });
			
            body.addClass('modal-opened');

			if(window.location.pathname.split('/')[2] == 'work'){
				$('#page-content').addClass('not-menu');
			}
            $.ajax({
                type: 'POST',
                url: dl_config.ajax_url,
                data: args,
                cache: false,
                success: function(data) {
                    //jQuery('#vehicle-value-box".$post->ID."').html(msg+',00€');
                    //}

                    //$.post(dl_config.ajax_url,args).done(function( data ) {
                    if (data != undefined) {
                        var modal = $(pageContentModalId);
                        var result = JSON.parse(data);

                        if (result.status) {

                            //console.log(result);

                            $('.page-content-inner', modal).html(result.html);

                            if(result.html_parts != undefined) {
                                if($('.get-in-touch-wrapper',modal).length == 0) {
                                    $(modal).append(result.html_parts.get_in_touch);
                                    $('.get-in-touch-wrapper',modal).show();
                                } else {
                                    $('.get-in-touch-wrapper',modal).html($(result.html_parts.get_in_touch).html()).show();
                                }
                            } else {
                                $('.get-in-touch-wrapper',modal).hide();
                            }

                            if(result.classes != undefined && result.classes != '') {
                                modal.attr('data-page',result.classes);
                            }

                            modal.slideDown(500);
	
							if (dl_config.blog_url == url) {
                                modal.slideUp(500);
                                body.removeClass('modal-opened');
								$('#page-content').removeClass('not-menu');
                            }

                            if (addEntry == true) {
                                // Add History Entry using pushState
                                history.pushState({
                                    url: url
                                }, null, url);

                                //console.log(window.history);
                            }

                            DigitalLight.contentModalLoad();
                            DigitalLight.loader(false);

                        } else {
                            alert('Error');
                        }
                    }
                }
            });
        },

        loader: function(status) {
            var $body = $('body');
            var loaderId = 'dl-loader';

            if(status) {
                $body.append('<div id="' + loaderId + '"><div class="loader-inner"><img src="' + dl_config.template_url + '/images/DL2k_opt.gif"></div></div>');
                $('#' + loaderId).fadeIn(500);
            } else {
                $('#' + loaderId).fadeOut(500, function() {
                   $(this).remove();
                });
            }
        },

        contentModalLoad: function() {
            DigitalLight.workCarouselInit();
        },

        workCarouselInit: function() {
            productCarousel = $(productCarouselId);
            productCarouselThumbnails = $(productCarouselThumbnailsId);

            if(productCarousel.length && productCarouselThumbnails.length) {
                productCarousel.owlCarousel({
                    singleItem: true,
                    slideSpeed: 1000,
                    navigation: false,
                    pagination: false,
                    autoHeight: true,
					loop: false,
                    afterAction: DigitalLight.carouselSyncPosition,
                    responsiveRefreshRate: 200,
                    /*autoHeight: true*/
					
                    afterInit: function(elem) {
                        var current = this.currentItem;
                        var slide = elem.find(".owl-item").eq(current);
                        var $frame = slide.find('iframe');

                        if($frame.length) {
                            var iframe = $frame[0];

                            setTimeout(function() {
                                var player = $f(iframe);
                                player.api('play');
                            },1000);
                        }

                    },
                    afterMove: function(elem) {

                        var current = this.currentItem;
                        var slide = elem.find(".owl-item").eq(current);
                        var $frame = slide.find('iframe');
						
                        DigitalLight.vimeoPauseAll();

                        if($frame.length) {
                            var iframe = $frame[0];
                            var player = $f(iframe);

                            player.api('play');
                        }

                        return false;

                        // When the player is ready, add listeners for pause, finish, and playProgress
                        player.addEvent('ready', function() {
                            status.text('ready');

                            player.addEvent('pause', onPause);
                            player.addEvent('finish', onFinish);
                            player.addEvent('playProgress', onPlayProgress);
                        });

                        // Call the API when a button is pressed
                        $('button',$(slide)).bind('click', function() {
                            player.api($(this).text().toLowerCase());
                        });

                        function onPause() {
                            status.text('paused');
                        }

                        function onFinish() {
                            status.text('finished');
                        }

                        function onPlayProgress(data) {
                            status.text(data.seconds + 's played');
                        }

                    }
                });

                productCarouselThumbnails.owlCarousel({
                    items: 9,
                    //items: 4,
                    itemsDesktop: [1199, 6],
                    itemsDesktopSmall: [979, 4],
                    itemsTablet: [768, 7],
                    itemsMobile: [479, 4],
                    navigation: false,
                    pagination: false,
					loop: false,
                    responsiveRefreshRate: 100,
                    navigationText:['<span class="lnr lnr-chevron-left"></span>','<span class="lnr lnr-chevron-right"></span>'],
                    afterInit: function (el) {
                        el.find(".owl-item").eq(0).addClass("synced");
                    }
                });

                productCarouselThumbnails.on("click", ".owl-item", function (e) {
                    e.preventDefault();
                    var number = $(this).data("owlItem");
                    productCarousel.trigger("owl.goTo", number);
                });
            }
        },

        vimeoSlidePlay: function (frame) {

        },

        vimeoPauseAll: function() {
            $('iframe[src*="vimeo.com"]').each(function () {
                $f(this).api('pause');
            });
        },

        carouselCenter: function(number){
            var sync2visible = productCarouselThumbnails.data("owlCarousel").owl.visibleItems;
            var num = number;
            var found = false;
            for(var i in sync2visible){
                if(num === sync2visible[i]){
                    var found = true;
                }
            }

            if(found===false){
                if(num>sync2visible[sync2visible.length-1]){
                    productCarouselThumbnails.trigger("owl.goTo", num - sync2visible.length+2)
                }else{
                    if(num - 1 === -1){
                        num = 0;
                    }
                    productCarouselThumbnails.trigger("owl.goTo", num);
                }
            } else if(num === sync2visible[sync2visible.length-1]){
                productCarouselThumbnails.trigger("owl.goTo", sync2visible[1])
            } else if(num === sync2visible[0]){
                productCarouselThumbnails.trigger("owl.goTo", num-1)
            }

        },

        carouselSyncPosition: function(el){
            var current = this.currentItem;
            productCarouselThumbnails
                .find(".owl-item")
                .removeClass("synced")
                .eq(current)
                .addClass("synced")
            if(productCarouselThumbnails.data("owlCarousel") !== undefined){
                DigitalLight.carouselCenter(current)
            }
			if (document.body.clientWidth > '991') {
				var linksArr = [];
				jQuery('.article-column-inner .article-item a').each(function() {
					linksArr.push($(this).attr('href').split('/')[5]);
				});
				
				var that = this;
				//START
				
				jQuery('.slide-manage-buttons a:nth-child(1)').click(function(e){
					if (that.currentItem === 0) {
						jQuery(this).attr('data-action', '');
						var currentUrl = window.location.pathname.split('/')[3],
							currentlocation = window.location.toString(),
							indexUrl = linksArr.indexOf(currentUrl);
							if(indexUrl != 0) {
								var prevUrl = linksArr[indexUrl - 1];
							} else {
								var prevUrl = linksArr[linksArr.length - 1];
							}
						
						jQuery('.list-articles-wrapper .article-item a').each(function() {
							if(jQuery(this).attr('href').indexOf(prevUrl) != -1) {
								jQuery(this).click();
								var timerId = setInterval(function(){
									if (currentlocation != window.location.toString() && jQuery('#work-carousel-thumbnails .owl-item').length > 0) {
										jQuery('#work-carousel .owl-wrapper').addClass('transition-0');
										jQuery('#work-carousel-thumbnails .owl-item').last().click();
									}
								}, 10);
								if (jQuery('#work-carousel-thumbnails .owl-item').hasClass('synced')) {
									setInterval(function() {
										jQuery('#work-carousel .owl-wrapper').removeClass('transition-0');
										clearInterval(timerId);
									}, 800);
								}
							}
						});
					}
				});
				//THE END
				jQuery('.slide-manage-buttons a:nth-child(2)').click(function(e){
					if (that.currentItem + 1 === that.itemsAmount) {
						jQuery(this).attr('data-action', '');
						var currentUrl = window.location.pathname.split('/')[3],
							indexUrl = linksArr.indexOf(currentUrl);
							if(indexUrl != linksArr.length - 1) {
								var nextUrl = linksArr[indexUrl + 1];
							} else {
								var nextUrl = linksArr[0];
							}
						
						jQuery('.list-articles-wrapper .article-item a').each(function() {
							if(jQuery(this).attr('href').indexOf(nextUrl) != -1) {
								jQuery(this).click();
							}
						});
					}
				});
			}
        },

        getViewport: function() {
            var e = window, a = 'inner';

            //console.log(window);
            //console.log(document);

            if (!('innerWidth' in window )) {
                a = 'client';
                e = document.documentElement || document.body;
            }

            return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
        },

        mobileEvents: function(status) {
            if(status) {
                $(toggleCategoryPostsAction).off().on('click', function() {
                    var that = $(this);
                    var column = that.closest('.article-column');
                    var wrapper = that.closest('.list-articles-wrapper');
                    var header = $('#header');

                    if($('.article-column-inner',column).css('display') == 'none') {
                        $('.article-column-inner',wrapper).slideUp(500);
                        $('.article-column-inner',column).slideDown(500, function() {
                            $("html, body").animate({
                                scrollTop: that.offset().top - header.innerHeight()
                            }, 500);
                        });

                    } else {
                        $('.article-column-inner',wrapper).slideUp(500);
                    }
                });

                $(mobileCarouselId).addClass('initialized').owlCarousel({
                    pagination: true,
                    navigation : false, // Show next and prev buttons
                    slideSpeed : 300,
                    paginationSpeed : 400,
                    singleItem:true
                })

            } else {
                $(toggleCategoryPostsAction).off();
                if($(mobileCarouselId).hasClass('initialized')) {
                    $(mobileCarouselId).data('owlCarousel').destroy();
                    $(mobileCarouselId).removeClass('initialized');
                }
            }
        }
    }
}();



// for portfolio slider type on Homepage
$(document).on('ready', function() {
    $('.article-carousel').owlCarousel({
        items : 1,
        autoPlay : 5000,
        pagination : false,
        navigation : false,
        paginationSpeed : 0,
        rewindSpeed : 0,
        singleItem : true,
        transitionStyle : "fade"
    });
    $('.article-carousel-mobile').owlCarousel({
        autoPlay : 5000,
        pagination : true,
        navigation : false,
        paginationSpeed : 0,
        rewindSpeed : 0,
        singleItem : true,
        transitionStyle : "fade"
    });
});


$(window).on('load resize', function() {
    var slideHeight = $('.list-articles-wrapper').find('.article-column').width();
    $('.list-articles-wrapper .article-carousel').parents('.article-item').css({'max-height' : slideHeight});
});



$(document).on('ready', function() {
    DigitalLight.loader(true);
});

$(window).on('load', function() {
    DigitalLight.init();
});

$(window).on('load resize', function() {
    if(DigitalLight.getViewport().width <= 991) {
        DigitalLight.mobileEvents(true);
    } else {
        DigitalLight.mobileEvents(false);
    }
});

$(window).on('load resize scroll', function() {
    DigitalLight.calcCategoryTitles();
});

jQuery( ".list-articles-wrapper .article-item img, .list-articles-wrapper .video-item video" ).click(function() {
	if (document.body.clientWidth > '991') {
		if (jQuery('#page-content').is(':hidden')) {
			jQuery('#footer .text-center a').click();
		}
	}
	jQuery('#page-content').addClass('not-menu');
});
jQuery(document).ready(function(){
	if (document.body.clientWidth > '991') {
		jQuery('<div class="overlay1"></div>').insertAfter('#page-content');
		jQuery('.overlay1').click(function(e){
			jQuery('#page-content .close-popup-link').click();
		});
	}
	jQuery('#header .main-navigation ul li.current-menu-item a').click(function(){
		jQuery('#page-content').removeClass('not-menu');
	});
});