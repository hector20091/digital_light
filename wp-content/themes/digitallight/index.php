<?php get_header();

$work_categories = get_terms(array(
    'taxonomy' => 'dl_work_categories',
    'hide_empty' => false
));

//unset($work_categories[0]);

//echo '<pre>';print_r($work_categories);echo '</pre>';

?>

    <main class="page-content-wrapper">

        <?php get_template_part('templates/parts/mobile-slider'); ?>

        <?php if(sizeof($work_categories) > 0) { ?>

            <div class="page-content-wrapper-inner">
                <div class="list-articles-wrapper clearfix">

                    <?php foreach ($work_categories as $category_single) {

                        $posts = get_posts(array(
                            'post_type' => 'dl_work',
                            'numberposts' => -1,
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'dl_work_categories',
                                    'field' => 'id',
                                    'terms' => $category_single->term_id,
                                    'include_children' => false
                                )
                            )
                        )); ?>

                        <div class="article-column">
                            <a href="javascript:;" data-action="toggleCategoryPosts" class="category-name text-uppercase"><span><?php echo $category_single->name; ?></span></a>
                            <div class="article-column-inner">

                                <?php if(sizeof($posts) > 0) {

                                    foreach ($posts as $post) { setup_postdata($post); ?>

                                        <?php get_template_part('templates/single/work'); ?>

                                    <?php } wp_reset_postdata(); ?>

                                <?php } else { ?>

                                    <div class="alert alert-info">No Posts in <?php echo $category_single->name; ?></div>

                                <?php } ?>

                                <a href="javascript:;" data-action="closeAccordionTab" class="mobile-close-accordion"><span class="lnr lnr-cross"></span></a>

                            </div>
                        </div>

                    <?php } ?>

                </div>

                <?php get_template_part('templates/parts/popup-wrapper'); ?>

            </div>

        <?php } ?>

    </main>

<?php get_footer(); ?>
