<?php

define( 'RWMB_DIR', get_template_directory() . '/includes/plugins/meta-box/' );
define( 'RWMB_URL', get_template_directory_uri() . '/includes/plugins/meta-box/' );

include 'includes/plugins/meta-box/meta-box.php';
include 'includes/plugins/meta-box-group/meta-box-group.php';
include 'includes/aq_resizer.php';
include 'includes/post-types.php';
include 'includes/shortcodes.php';
include 'includes/dl-meta-boxes.php';

/**
 * Digital Light functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Digital_Light
 * @since Digital Light 1.0
 */

if ( ! function_exists( 'digital_light_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since Digital Light 1.0
 */
function digital_light_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on digital_light, use a find and replace
	 * to change 'digital_light' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'digital_light', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'digital_light' ),
		'social'  => __( 'Social Links Menu', 'digital_light' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );
}
endif; // digital_light_setup
add_action( 'after_setup_theme', 'digital_light_setup' );

/**
 * Register widget area.
 *
 * @since Digital Light 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function digital_light_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'digital_light' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'digital_light' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'digital_light_widgets_init' );

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Digital Light 1.1
 */
function digital_light_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'digital_light_javascript_detection', 0 );

/**
 * Enqueue scripts and styles.
 *
 * @since Digital Light 1.0
 */
function digital_light_scripts() {

    wp_deregister_script('jquery');

    wp_register_script('froogaloop', 'https://f.vimeocdn.com/js/froogaloop2.min.js', array(), '2.0', false);

    wp_register_script('jquery', get_template_directory_uri() . '/js/jquery.min.js', array(), '1.11.3', true);
    wp_register_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.6', true);
    wp_register_script('owl-carousel', get_template_directory_uri() . '/assets/owl-carousel/owl.carousel.min.js', array('jquery'), '3.3.6', true);
    // wp_register_script('owl-carousel-2.0.0', get_template_directory_uri() . '/assets/owl.carousel.2.0.0/owl.carousel.min.js', array('jquery'), '2.0.0', true);
    wp_register_script('fancybox', get_template_directory_uri() . '/assets/fancybox/jquery.fancybox.js', array('jquery'), '2.1.5', true);
    wp_register_script('fancybox-media', get_template_directory_uri() . '/assets/fancybox/helpers/jquery.fancybox-media.js', array('jquery','fancybox'), '2.1.5', true);
    wp_register_script('video-js', '//vjs.zencdn.net/5.4.6/video.min.js', array(), '5.10.2', true);
    wp_register_script('dl-theme', get_template_directory_uri() . '/js/theme.js', array('jquery'), '1.0', true);

    // Scripts
    wp_enqueue_script('froogaloop');
    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap');
    wp_enqueue_script('owl-carousel');
    // wp_enqueue_script('owl-carousel-2.0.0');
    wp_enqueue_script('fancybox');
    wp_enqueue_script('video-js');
    wp_enqueue_script('dl-theme');

    // Fonts
    wp_enqueue_style( 'font-avenir', get_template_directory_uri() . '/fonts/avenir/stylesheet.css');
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/fonts/font-awesome/css/font-awesome.min.css');
    wp_enqueue_style( 'font-linearicons', get_template_directory_uri() . '/fonts/font-linearicons/style.css');

    // Styles
    wp_enqueue_style( 'video-js', '//vjs.zencdn.net/5.4.6/video-js.min.css', array(), '3.3.6' );
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.6' );
    wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/assets/fancybox/jquery.fancybox.css', array(), '2.1.5' );
    wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/assets/owl-carousel/owl.carousel.css', array(), '3.3.6' );
    wp_enqueue_style( 'owl-carousel-transitions', get_template_directory_uri() . '/assets/owl-carousel/owl.transitions.css', array(), '3.3.6' );
    wp_enqueue_style( 'owl-carousel-theme', get_template_directory_uri() . '/assets/owl-carousel/owl.theme.css', array('owl-carousel'), '3.3.6' );
    // wp_enqueue_style( 'owl-carousel-2.0.0', get_template_directory_uri() . '/assets/owl.carousel.2.0.0/assets/owl.carousel.css', array(), '2.0.0' );
    wp_enqueue_style( 'dl-theme', get_template_directory_uri() . '/css/digital-light-theme.css', array(), '1.0' );

	// Load our main stylesheet.
	wp_enqueue_style( 'digital_light-style', get_stylesheet_uri() );

	wp_localize_script( 'dl-theme', 'dl_config', array(
		'ajax_url'   => admin_url('admin-ajax.php'),
        'blog_url'   => site_url('/'),
        'template_url'   => get_template_directory_uri(),
	) );
}
add_action( 'wp_enqueue_scripts', 'digital_light_scripts' );

function get_page_id_by_template_name($template = '') {
    if(!empty($template)) {
        $template = $template.'.php';
        $page_info = get_posts(array(
            'post_type'   => 'page',
            'meta_query' => array(
                array(
                    'key' => '_wp_page_template',
                    'value' => $template
                )
            )
        )) ;

        wp_reset_query();
        wp_reset_postdata();

        return $page_info[0]->ID;
    }
}

function theme_get_template( $template = '', $args = array(), $echo = true ) {
    ob_start();
    include TEMPLATEPATH . '/' . $template . '.php';
    $content = ob_get_contents();
    ob_end_clean();

    if($echo) {
        echo $content;
    } else {
        return $content;
    }
}

add_filter('body_class','dl_body_class');
function dl_body_class( $classes ) {

    $classes[] = 'header-fixed';

    return $classes;
}

add_action( 'wp_ajax_get_content', 'get_content_callback' );
add_action( 'wp_ajax_nopriv_get_content', 'get_content_callback' );
function get_content_callback() {
    global $post;

    //echo '<pre>';print_r($post);echo '</pre>';die;

    $result = array();

    if(isset($_POST['url'])) {
        $post_id = url_to_postid( $_POST['url'] );

        //echo get_post_type($post_id);

        //echo '<pre>';print_r(get_post($post_id));echo '</pre>';

        //$result['html'] = '<pre>';print_r(get_post_meta($post_id));echo '</pre>';

        $args = array(
            'post_id' => $post_id
        );

        //echo '<pre>';print_r($args);echo '</pre>';

        //wp( 'p=' . $post_id );

        //wp( 'p=' . $post_id );

        if(get_post_type($post_id) == 'dl_work') {
            //wp( 'p=' . $post_id . '&post_type=dl_work' );
            //$result['html'] = '<pre>';print_r(get_post_meta($post_id));echo '</pre>';
            $result['html'] = theme_get_template('templates/single-work',$args,false);
            $result['classes'] = 'portfolio-work';
        } else {
            $result['classes'] = 'page-template';

            $page_template = get_post_meta($post_id,'dl_template_select',true);
            $get_in_touch_visibility = get_post_meta($post_id,'dl_display_get_in_touch',true);

            if($get_in_touch_visibility) {
                $result['html_parts'] = array(
                    'get_in_touch' => theme_get_template('templates/parts/get_in_touch', array(
                        'post_id' => $post_id,
                        'visibility' => $get_in_touch_visibility
                    ), false)
                );
            }

            if(!empty($page_template)) {
                $result['html'] = theme_get_template($page_template,$args,false);
            } else {
                $result['html'] = theme_get_template('templates/page-default',$args,false);
            }
        }

        $result['status'] = true;

    } else {
        $result['status'] = false;
    }

    die(json_encode($result));
}

//add_action( 'template_redirect', 'wpse_76802_maintance_mode' );

function wpse_76802_maintance_mode() {
    if ( ! is_front_page() ) {
        wp_redirect( home_url(), 301 );
        exit;
    }
}

function thankyou_page_rewrite_rule() {
    add_rewrite_rule( '^thankyou\/(.*)\/?', "index.php", 'top' );
    flush_rewrite_rules();
}
//add_action('init', 'thankyou_page_rewrite_rule');

function page_templates() {
    return array(
        'templates/contact' => 'Contact',
        'templates/clients' => 'Clients',
        'templates/columns' => 'Columns',
        'templates/login' => 'Login'
    );
}


add_image_size( 'mobile-slider', 720, 560, false );
