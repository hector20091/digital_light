<?php global $post;

$page_id = $args['post_id'];

wp( 'p=' . $page_id . '&post_type=page' );

$map_url = get_post_meta($page_id,'dl_contact_map_url',true); ?>


<div class="contact-columns-wrapper">

    <div class="login-table-wrapper">

        <div class="row">
            <div class="col-lg-4 col-lg-push-4 col-md-6 col-md-push-3">

                <form action="#" method="post" class="form-login">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="USER NAME" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="PASSWORD" autocomplete="off">
                    </div>
                    <input type="submit" class="btn btn-primary btn-block" value="LOGIN">
                </form>

            </div>
        </div>

    </div>

</div>