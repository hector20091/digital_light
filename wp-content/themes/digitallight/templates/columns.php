<?php

$page_id = $args['post_id'];

// Column 1
$image_id_col1 = get_post_meta($page_id,'dl_col_1_image',true);
$subheading_col1 = get_post_meta($page_id,'dl_col_1_subheading',true);
$desc_col1 = get_post_meta($page_id,'dl_col_1_description',true);

// Column 2
$image_id_col2 = get_post_meta($page_id,'dl_col_2_image',true);
$desc_col2 = get_post_meta($page_id,'dl_col_2_description',true);

// Column 3
$image_id_col3 = get_post_meta($page_id,'dl_col_3_image',true);
$desc_col3 = get_post_meta($page_id,'dl_col_3_description',true);

?>

<div class="template-columns-wrapper">
    <div class="row">

        <?php if (wp_is_mobile()) {
            $slides = get_field('mobile-slider', 40);
            if(sizeof($slides) > 0) { ?>
            <div class="mobile-carousel-about-wrapper">
                <div id="mobile-carousel-about" class="owl-carousel owl-theme mobile-carousel-box">

                    <?php foreach ($slides as $slide) { ?>
                        <div class="item">
                            <img src="<?php echo aq_resize($slide['img'], 768, 453, true, true, true); ?>" alt="" class="mobile-img">
                        </div>
                    <?php } ?>

                </div>
            </div>
            <script>
                $('#mobile-carousel-about').addClass('initialized').owlCarousel({
                    pagination: true,
                    navigation : false, // Show next and prev buttons
                    slideSpeed : 300,
                    paginationSpeed : 400,
                    singleItem : true,
                    autoHeight : true
                })
            </script>
            <style>
                .template-columns-wrapper img:not(.mobile-img) {
                    display: none;
                }
            </style>
        <?php } } ?>

        <div class="col-md-5">
            <?php if(!empty($image_id_col1)) {
                $url = wp_get_attachment_image_src($image_id_col1,'full'); ?>

                <div class="columns-thumb">
                    <img src="<?php echo $url[0]; ?>">
                </div>

            <?php } ?>

            <div class="column-description">
                <div class="column1-description-wrapper">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-4">
                            <h2><?php echo $subheading_col1; ?></h2>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-8">
                            <?php echo $desc_col1; ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-4">
            <?php if(!empty($image_id_col2)) {
                $url = wp_get_attachment_image_src($image_id_col2,'full'); ?>

                <div class="columns-thumb">
                    <img src="<?php echo $url[0]; ?>">
                </div>

            <?php } ?>

            <div class="column-description">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9">
                        <?php echo $desc_col2; ?>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-3">
            <?php if(!empty($image_id_col3)) {
                $url = wp_get_attachment_image_src($image_id_col3,'full'); ?>

                <div class="columns-thumb">
                    <img src="<?php echo $url[0]; ?>">
                </div>

            <?php } ?>

            <div class="column-description">
                <?php echo $desc_col3; ?>
            </div>

        </div>
    </div>
</div>