<?php $links = ot_get_option('social_links');

if(sizeof($links) > 0) { ?>

    <ul class="social-links">

        <?php foreach ($links as $social) { ?>

            <li><a title="<?php echo $social['name']; ?>" href="<?php echo $social['href']; ?>"><i class="fa <?php echo $social['title']; ?>"></i></a></li>

        <?php } ?>

    </ul>

<?php } ?>