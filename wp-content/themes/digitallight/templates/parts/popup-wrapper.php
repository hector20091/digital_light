<div id="page-content" class="content-inner-page-wrapper">

    <a href="javascript:" data-action="closeModalWindow" class="close-popup-link"><span class="lnr lnr-cross"></span></a>

    <div class="container">
        <div class="page-content-inner"></div>
    </div>
</div>