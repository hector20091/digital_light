<?php global $post;

$content = ot_get_option('get_in_touch_content');

if(!empty($content) && $args['visibility'] == 1) { ?>

    <div class="get-in-touch-wrapper">
        <div class="container">

            <?php echo $content; ?>

        </div>
    </div>

<?php } ?>