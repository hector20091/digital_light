<?php
$slides = get_posts(array(
    'post_type' => 'mobile_slides',
    'numberposts' => -1
));

if(sizeof($slides) > 0) { ?>

    <div class="mobile-carousel-wrapper">
        <div id="mobile-carousel" class="owl-carousel owl-theme mobile-carousel-box">

            <?php foreach ($slides as $post) { setup_postdata($post);
                $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
                $thumbnail_img = aq_resize($image_url[0],545,545,true,true,true); ?>

                <div class="item">
                    <img src="<?php echo $thumbnail_img; ?>">
                </div>

            <?php } wp_reset_postdata(); ?>

        </div>
    </div>

<?php } wp_reset_query(); ?>