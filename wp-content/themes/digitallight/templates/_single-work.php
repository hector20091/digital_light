<?php global $post, $wp, $wp_query;

$page_id = $args['post_id'];

$gallery = array();

$post_thumbnail_id = get_post_thumbnail_id( $page_id );
$type = get_post_meta($page_id,'dl_protfolio_type',true);
$photographer = get_post_meta($page_id,'dl_protfolio_photographer',true);
$client = get_post_meta($page_id,'dl_protfolio_client',true);
$galleryIds = get_post_meta($page_id,'dl_protfolio_gallery');
$categories_obj = wp_get_post_terms($page_id,'dl_work_categories');
$categories = array();

$portfolio_types = rwmb_meta( 'portfolio_types', array(), $page_id );

echo '<pre>';print_r($portfolio_types);echo '</pre>';

//echo '<pre>';print_r($type);echo '</pre>';

if(sizeof($categories_obj) > 0) {
    foreach ($categories_obj as $category_single) {
        $categories[] = $category_single->name;
    }

}

if(!empty($post_thumbnail_id)) {
    $gallery[] = $post_thumbnail_id;
}

if(!empty($galleryIds) && sizeof($galleryIds) > 0) {
    $gallery = array_merge($gallery,$galleryIds);
}

if(sizeof($gallery) > 0) {?>

    <div class="custom-slide-image-wrapper">
        <div class="slide-manage-buttons-wrapper">
            <div class="slide-manage-buttons">
                <a href="javascript:;" data-action="customSlide" data-direction="prev"><!--<span class="lnr lnr-chevron-left"></span>--></a>
                <a href="javascript:;" data-action="customSlide" data-direction="next"><!--<span class="lnr lnr-chevron-right"></span>--></a>
            </div>
        </div>

        <div id="work-carousel" class="owl-carousel owl-theme work-carousel-box">

            <?php foreach ($gallery as $image_single_id) {
                $image_url = wp_get_attachment_image_src($image_single_id,'full');
                //$thumbnail_img = aq_resize($thumbnail_url,545,545,true,true,true); ?>

                <div class="item">
                    <img src="<?php echo $image_url[0]; ?>">
                </div>

            <?php } ?>

        </div>

    </div>

    <div class="work-description-columns-wrapper">
        <div class="row">
            <div class="col-sm-6">
                <div class="work-info">

                    <?php if(sizeof($categories) > 0) { ?>

                        <div class="category text-uppercase"><?php echo join(', ',$categories); ?></div>

                    <?php } ?>

                    <?php if(!empty($photographer)) { ?>

                        <div class="meta-data">Photographer <span><?php echo $photographer; ?></span></div>

                    <?php } ?>

                    <?php if(!empty($client)) { ?>

                        <div class="meta-data">Clients <span><?php echo $client; ?></span></div>

                    <?php } ?>

                </div>
            </div>
            <div class="col-sm-6">

                <div id="work-carousel-thumbnails" class="owl-carousel owl-theme work-carousel-thumbnails-box">

                    <?php foreach ($gallery as $image_single_id) {
                        $image_url = wp_get_attachment_image_src($image_single_id,'full');
                        $thumbnail_img = aq_resize($image_url[0],148,148,true,true,true); ?>

                        <div class="item">
                            <img src="<?php echo $thumbnail_img; ?>">
                        </div>

                    <?php } ?>

                </div>

            </div>
        </div>
    </div>

<?php } ?>