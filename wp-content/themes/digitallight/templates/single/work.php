<?php $page_id = get_the_ID();

$photographer = get_post_meta($page_id,'dl_protfolio_photographer', true);
$client = get_post_meta($page_id,'dl_protfolio_client', true);
$type = get_post_meta($page_id,'dl_protfolio_display_listing',true);


if (wp_is_mobile() || wpmd_is_device()) {

    if ($type == 'gallery' || $type == 'slider') {

        $gallery = array();

        $portfolio_types = rwmb_meta( 'portfolio_types', array(), $page_id );
        if(!empty($portfolio_types) && sizeof($portfolio_types) > 0) {
            foreach ($portfolio_types as $p_type) {
                if($p_type['dl_protfolio_type'] == 'video') {
                    $image_url = wp_get_attachment_image_src($p_type['dl_protfolio_gallery'][0],'full');
                    $gallery[] = array(
                        'type'=> $p_type['dl_protfolio_type'],
                        'video_url' => $p_type['dl_portfolio_vimeo_url'],
                        'image_url' => $image_url[0]
                    );
                } else {
                    if(!empty($p_type['dl_protfolio_gallery']) && sizeof($p_type['dl_protfolio_gallery']) > 0) {
                        $image_url = wp_get_attachment_image_src($p_type['dl_protfolio_gallery'][0],'full');
                        $gallery[] = array(
                            'type' => $p_type['dl_protfolio_type'],
                            'url' => $image_url[0]
                        );
                    }
                }
            }
        } ?>

        <div class="article-item">
            <div id="article-carousel-<?php echo $page_id; ?>" class="article-carousel-mobile mobile-carousel-box">
                <?php foreach ($gallery as $image_single_arr) { ?>

                    <?php if($image_single_arr['type'] == 'image') {
                        $thumbnail_img = aq_resize($image_single_arr['url'],768,768,true,true,true); ?>

                        <div class="item">
                            <img src="<?php echo $thumbnail_img; ?>">
                        </div>

                    <?php } else {
                        $thumbnail_img = aq_resize($image_single_arr['image_url'],768,768,true,true,true); ?>

                        <div class="item">
                            <img src="<?php echo $thumbnail_img; ?>">
                        </div>

                    <?php } ?>
                <?php } ?>
            </div>

            <div class="article-info mob">
                <ul>
                    <?php if(!empty($photographer)) { ?>
                        <li>Photographer <span><?php echo $photographer; ?></span></li>
                    <?php } ?>
                    <?php if(!empty($client)) { ?>
                        <li>Client <span><?php echo $client; ?></span></li>
                    <?php } ?>
                </ul>
            </div>
        </div>

    <?php } else if($type == 'video') {
        $preview_id = get_post_meta($page_id,'dl_portfolio_video_preview',true);
        $vimeo_url = get_post_meta($page_id,'dl_portfolio_vimeo_url',true);
        $video_poster_id = get_post_meta($page_id,'dl_portfolio_video_poster',true);

        $preview_url = wp_get_attachment_url($preview_id);
        $video_poster_url = wp_get_attachment_url($video_poster_id);

        $thumbnail_img = '';
        if(!empty($video_poster_url)) {
            $thumbnail_img = aq_resize($video_poster_url,768,768,true,true,true);
        }

        $video_id = 'video_' . get_the_ID() . '_' . $preview_id; ?>

        <!--<div class="video-item" data-action="videoAutoPlay" data-id="<?php /*echo $video_id; */?>" data-vimeo_url="<?php /*echo $vimeo_url; */?>">-->
        <div class="video-item dynamic-load" data-href="<?php the_permalink(); ?>" data-action="videoAutoPlay" data-id="<?php echo $video_id; ?>" data-vimeo_url="<?php echo $vimeo_url; ?>">
            <img src="<?php echo $thumbnail_img; ?>" alt="">
            <div class="video-container">
                <video id="<?php echo $video_id; ?>" class="video-js" preload="auto" width="100%" height="100%" poster="<?php echo $thumbnail_img; ?>" data-setup='{ "controls": false, "autoplay": true, "preload": "auto", "loop": true }'>
                    <source src="<?php echo $preview_url; ?>" type='video/mp4'>
                    <p class="vjs-no-js">
                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                    </p>
                </video>
            </div>

            <div class="article-info mob">
                <ul>
                    <?php if(!empty($photographer)) { ?>
                        <li>Photographer <span><?php echo $photographer; ?></span></li>
                    <?php } ?>
                    <?php if(!empty($client)) { ?>
                        <li>Client <span><?php echo $client; ?></span></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    <?php } ?>

<?php } else {

    if($type == 'gallery') {

        $preview_img = get_post_meta($page_id,'dl_protfolio_preview_image',true);

        if(has_post_thumbnail() || $preview_img) {

            if ($preview_img) {
                $thumbnail_url = wp_get_attachment_image_src( $preview_img, 'full' );
                $thumbnail_img = aq_resize($thumbnail_url[0],546,546,true,true,true);
            } else {
                $thumbnail_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
                $thumbnail_img = aq_resize($thumbnail_url,546,546,true,true,true);
            } ?>

            <div class="article-item">
                <a href="<?php the_permalink(); ?>" class="dynamic-load"><img src="<?php echo $thumbnail_img; ?>" alt="<?php the_title(); ?>"></a>
                <div class="article-info">
                    <ul>

                        <?php if(!empty($photographer)) { ?>

                            <li>Photographer <span><?php echo $photographer; ?></span></li>

                        <?php } ?>

                        <?php if(!empty($client)) { ?>

                            <li>Client <span><?php echo $client; ?></span></li>

                        <?php } ?>

                    </ul>
                </div>
            </div>

        <?php } ?>

    <?php } else if($type == 'slider') {

        $gallery = array();
        $i = 0;
        $preview_imgs = get_post_meta($page_id,'dl_protfolio_preview_image');

        if (!empty($preview_imgs) && sizeof($preview_imgs)>0) {

            foreach ($preview_imgs as $p_img) {
                if ($i < 3) {
                    $image_url = wp_get_attachment_image_src( $p_img, 'full' );
                    $gallery[] = array(
                        'type' => 'image',
                        'url' => $image_url[0]
                    );
                    $i++;
                }
            }

        } else {

            $portfolio_types = rwmb_meta( 'portfolio_types', array(), $page_id );
            if(!empty($portfolio_types) && sizeof($portfolio_types) > 0) {
                foreach ($portfolio_types as $p_type) {
                    if ($i < 3) {
                        if($p_type['dl_protfolio_type'] == 'video') {
                            $image_url = wp_get_attachment_image_src($p_type['dl_protfolio_gallery'][0],'full');
                            $gallery[] = array(
                                'type'=> $p_type['dl_protfolio_type'],
                                'video_url' => $p_type['dl_portfolio_vimeo_url'],
                                'image_url' => $image_url[0]
                            );
                        } else {
                            if(!empty($p_type['dl_protfolio_gallery']) && sizeof($p_type['dl_protfolio_gallery']) > 0) {
                                $image_url = wp_get_attachment_image_src($p_type['dl_protfolio_gallery'][0],'full');
                                $gallery[] = array(
                                    'type' => $p_type['dl_protfolio_type'],
                                    'url' => $image_url[0]
                                );
                            }
                        }
                        $i++;
                    }
                }
            }

        } ?>

        <div class="article-item">
            <a href="<?php the_permalink(); ?>" class="dynamic-load">
                <div id="article-carousel-<?php echo $page_id; ?>" class="article-carousel">
                    <?php foreach ($gallery as $image_single_arr) { ?>

                        <?php if($image_single_arr['type'] == 'image') {
                            $thumbnail_img = aq_resize($image_single_arr['url'],546,546,true,true,true); ?>

                            <div class="item">
                                <img src="<?php echo $thumbnail_img; ?>">
                            </div>

                        <?php } else {
                            $thumbnail_img = aq_resize($image_single_arr['image_url'],546,546,true,true,true); ?>

                            <div class="item">
                                <img src="<?php echo $thumbnail_img; ?>">
                            </div>

                        <?php } ?>
                    <?php } ?>
                </div>
            </a>

            <div class="article-info">
                <ul>
                    <?php if(!empty($photographer)) { ?>
                        <li>Photographer <span><?php echo $photographer; ?></span></li>
                    <?php } ?>
                    <?php if(!empty($client)) { ?>
                        <li>Client <span><?php echo $client; ?></span></li>
                    <?php } ?>
                </ul>
            </div>
        </div>

    <?php } else if($type == 'video') {
        $preview_id = get_post_meta($page_id,'dl_portfolio_video_preview',true);
        $vimeo_url = get_post_meta($page_id,'dl_portfolio_vimeo_url',true);
        $video_poster_id = get_post_meta($page_id,'dl_portfolio_video_poster',true);

        $preview_url = wp_get_attachment_url($preview_id);
        $video_poster_url = wp_get_attachment_url($video_poster_id);

        $thumbnail_img = '';
        if(!empty($video_poster_url)) {
            $thumbnail_img = aq_resize($video_poster_url,546,546,true,true,true);
        }

        $video_id = 'video_' . get_the_ID() . '_' . $preview_id; ?>

        <!--<div class="video-item" data-action="videoAutoPlay" data-id="<?php /*echo $video_id; */?>" data-vimeo_url="<?php /*echo $vimeo_url; */?>">-->
        <div class="video-item dynamic-load" data-href="<?php the_permalink(); ?>" data-action="videoAutoPlay" data-id="<?php echo $video_id; ?>" data-vimeo_url="<?php echo $vimeo_url; ?>">
            <img src="<?php echo $thumbnail_img; ?>" alt="">
            <div class="video-container">
                <video id="<?php echo $video_id; ?>" class="video-js" preload="auto" width="100%" height="100%" poster="<?php echo $thumbnail_img; ?>" data-setup='{ "controls": false, "autoplay": true, "preload": "auto", "loop": true }'>
                    <source src="<?php echo $preview_url; ?>" type='video/mp4'>
                    <!--<source src="MY_VIDEO.webm" type='video/webm'>-->
                    <p class="vjs-no-js">
                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                    </p>
                </video>
            </div>

            <div class="article-info">
                <ul>
                    <?php if(!empty($photographer)) { ?>
                        <li>Photographer <span><?php echo $photographer; ?></span></li>
                    <?php } ?>
                    <?php if(!empty($client)) { ?>
                        <li>Client <span><?php echo $client; ?></span></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    <?php } ?>

<?php } ?>