<?php global $post;

$page_id = $args['post_id'];

wp( 'p=' . $page_id . '&post_type=page' );

$lat = get_post_meta($page_id,'dl_contact_lat_map',true);
$lng = get_post_meta($page_id,'dl_contact_lng_map',true);
$zoom = get_post_meta($page_id,'dl_contact_zoom_map',true);


?>


<div class="contact-columns-wrapper">
    <div class="contact-table">

        <?php while ( have_posts() ) : the_post(); ?>

        <div>

            <?php the_content(); ?>

        </div>

        <?php endwhile; ?>

        <?php if(!empty($lat) && !empty($lng) && !empty($zoom)) { ?>

            <div id="contact-map" style="height: 450px;width: 100%;"></div>


            <!-- <div>
                <iframe src="<?php echo $map_url; ?>" width="600" height="450" frameborder="0" style="border:0;width:100%;" allowfullscreen></iframe>
            </div> -->

            <script type="text/javascript">
                var map;
                var marker;
                var infowindow;

                //;

                initialize_this_map();

                function initialize_this_map() {
                    var myLatlng = new google.maps.LatLng(<?php echo $lat; ?>,<?php echo $lng; ?>);
                  <?php if (wp_is_mobile() || wpmd_is_phone()) { ?>
                    var centerLatlng = new google.maps.LatLng(<?php echo $lat; ?>,<?php echo $lng; ?>);
                  <?php } else { ?>
                    var centerLatlng = new google.maps.LatLng(<?php echo $lat; ?>,<?php echo $lng; ?>-0.0015);
                  <?php } ?>

                    var myOptions = {
                        zoom: <?php echo $zoom; ?>,
                        center: centerLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        scrollwheel: false
                    };

                    map = new google.maps.Map(document.getElementById("contact-map"), myOptions);

                    /*infowindow = new google.maps.InfoWindow({
                     content: '<?php //echo WPP_F::google_maps_infobox($post); ?>',
                     maxWidth: 500
                     });*/

                    marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                        //title: '<?php echo addslashes($post->post_title); ?>',
                        icon: {
                            url: '<?php echo get_template_directory_uri() . "/images/location_drop.png"; ?>',
                            size: new google.maps.Size(44, 64),
                            scaledSize: new google.maps.Size(44, 64)
                        }
                    });

                    /*google.maps.event.addListener(infowindow, 'domready', function() {
                     document.getElementById('infowindow').parentNode.style.overflow='hidden';
                     document.getElementById('infowindow').parentNode.parentNode.style.overflow='hidden';
                     });

                     setTimeout("infowindow.open(map,marker);",1000);*/
                }

            </script>

        <?php } ?>

    </div>
</div>