<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>

    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=true"></script>

</head>

<body <?php body_class(); ?>>

<header id="header">
    <div class="container-fluid">
        <a href="<?php bloginfo('url') ?>" class="logo overlay-menu">
            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="">
        </a>

        <a href="javascript:;" class="mobile-menu-toggle-link" data-action="toggleMobileMenu" data-anchor="#mobile-menu-wrapper"><span class="lnr lnr-menu"></span></a>

        <div id="mobile-menu-wrapper" class="mobile-menu-wrapper">

            <?php
            wp_nav_menu( array(
                'theme_location' => 'primary',
                'menu_class'     => 'primary-menu',
                'container' => 'nav',
                'container_class' => 'main-navigation text-uppercase'
            ) );
            ?>

            <div class="header-right-wrapper overlay-menu">

                <?php get_template_part('templates/parts/social-links'); ?>

            </div>

        </div>
    </div>
</header>