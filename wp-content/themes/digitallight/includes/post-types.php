<?php

function portfolio_post_type_init() {
    $args = array(
        'labels' => array(
            'name' => __( 'Portfolio' ),
            'singular_name' => __( 'work' )
        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'work' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail')
    );

    register_post_type( 'dl_work', $args );

    register_taxonomy(
        'dl_work_categories',
        'dl_work',
        array(
            'label' => __( 'Categories' ),
            'rewrite' => array( 'slug' => 'portfolio-cat' ),
            'hierarchical' => true,
        )
    );
}

function services_post_type_init() {
    $args = array(
        'labels' => array(
            'name' => __( 'Services' ),
            'singular_name' => __( 'service' )
        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'services' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor' )
    );

    register_post_type( 'mg_service', $args );

    register_taxonomy(
        'mg_service_cat',
        'mg_service',
        array(
            'hierarchical'  => true,
            'label'         => __( 'Categories' ),
            'singular_name' => __( 'Category' ),
            'rewrite'       => array( 'slug' => 'service-cat' ),
        )
    );
}

function team_post_type_init() {
    $args = array(
        'labels' => array(
            'name' => __( 'Team' ),
            'singular_name' => __( 'team' )
        ),
        'public'             => true,
        'publicly_queryable' => false,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => false,
        'rewrite'            => array( 'slug' => 'team' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail' )
    );

    register_post_type( 'mg_team', $args );
}

function clients_post_type_init() {
    $args = array(
        'labels' => array(
            'name' => __( 'Clients' ),
            'singular_name' => __( 'clients' )
        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'clients' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'thumbnail' )
    );

    register_post_type( 'dl_clients', $args );
}

function mobile_slides_post_type_init() {
    $args = array(
        'labels' => array(
            'name' => __( 'Mobile Slides' ),
            'singular_name' => __( 'mobile_slides' )
        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'mobile_slides' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'thumbnail' )
    );

    register_post_type( 'mobile_slides', $args );
}

//add_action( 'init', 'tutorials_post_type_init' );
//add_action( 'init', 'faq_post_type_init' );
//add_action( 'init', 'quizzes_post_type_init' );
//add_action( 'init', 'classification_post_type_init' );
add_action( 'init', 'portfolio_post_type_init' );
add_action( 'init', 'mobile_slides_post_type_init' );
add_action( 'init', 'clients_post_type_init' );
//add_action( 'init', 'case_studies_post_type_init' );
//add_action( 'init', 'services_post_type_init' );
//add_action( 'init', 'team_post_type_init' );