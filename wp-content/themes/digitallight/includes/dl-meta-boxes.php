<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://metabox.io/docs/registering-meta-boxes/
 */
add_filter( 'rwmb_meta_boxes', 'dl_register_meta_boxes' );
/**
 * Register meta boxes
 *
 * Remember to change "your_prefix" to actual prefix in your project
 *
 * @param array $meta_boxes List of meta boxes
 *
 * @return array
 */
function dl_register_meta_boxes( $meta_boxes ) {

    if ( isset( $_GET['post'] ) )
        $post_id = intval( $_GET['post'] );
    elseif ( isset( $_POST['post_ID'] ) )
        $post_id = intval( $_POST['post_ID'] );
    else
        $post_id = false;

    $post_id = (int) $post_id;
    $current_template = '';

    if($post_id != 0) {
        $current_template = get_post_meta($post_id,'dl_template_select',true);
    }

    /**
     * prefix of meta keys (optional)
     * Use underscore (_) at the beginning to make keys hidden
     * Alt.: You also can make prefix empty to disable it
     */
    // Better has an underscore as last sign
    $prefix = 'dl_';

    // Banner Meta Box
    $meta_boxes[] = array(
        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id'         => 'portfolio',
        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title'      => __( 'Portfolio Options', 'digital_light' ),
        // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
        'post_types' => array( 'dl_work' ),
        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context'    => 'normal',
        // Order of meta box: high (default), low. Optional.
        'priority'   => 'high',
        // Auto save: true, false (default). Optional.
        'autosave'   => true,
        // List of meta fields
        'fields'     => array(
            array(
                'name' => __( 'Display on Listing as', 'digital_light' ),
                'id'   => "{$prefix}protfolio_display_listing",
                'type' => 'select',
                'options' => array(
                    'gallery' => 'Image',
                    'slider' => 'Multiple images',
                    'video' => 'Video'
                )
            ),
            // DIVIDER
            array(
                'type' => 'divider',
                'id'   => 'portfolio_gallery_settings', // Not used, but needed
            ),
            array(
                'name' => __( 'Photographer', 'digital_light' ),
                'id'   => "{$prefix}protfolio_photographer",
                'type' => 'text'
            ),
            array(
                'name' => __( 'Client', 'digital_light' ),
                'id'   => "{$prefix}protfolio_client",
                'type' => 'text'
            ),

            array(
                'name'             => __( 'Preview Images', 'digital_light' ),
                'id'               => "{$prefix}protfolio_preview_image",
                'type'             => 'image_advanced'
            ),
            // DIVIDER
            array(
                'type' => 'divider',
                'id'   => 'portfolio_video_settings', // Not used, but needed
            ),
            // OEMBED
           /* array(
                'name' => __( 'Vimeo URL', 'digital_light' ),
                'id'   => "{$prefix}portfolio_vimeo_url",
                'desc' => __( 'Vimeo URL Example: https://vimeo.com/57421694', 'digital_light' ),
                'type' => 'oembed',
            ),*/
            // FILE ADVANCED (WP 3.5+)
            array(
                'name'             => __( 'Video Preview', 'digital_light' ),
                'id'               => "{$prefix}portfolio_video_preview",
                'type'             => 'file_advanced',
                'max_file_uploads' => 1,
                'mime_type'        => 'video'
            ),
            array(
                'name'             => __( 'Video Poster', 'digital_light' ),
                'id'               => "{$prefix}portfolio_video_poster",
                'type'             => 'file_advanced',
                'max_file_uploads' => 1,
                'mime_type'        => 'image'
            ),
            array(
                 'type' => 'divider',
                 'id'   => 'gallery_settings', // Not used, but needed
            ),
            array(
                'type' => 'divider',
                'id'   => 'gallery_settings2', // Not used, but needed
            ),
            array(
                'type' => 'divider',
                'id'   => 'gallery_settings3', // Not used, but needed
            ),

            array(
                'id'     => 'portfolio_types',
                // Group field
                'type'   => 'group',
                // Clone whole group?
                'clone'  => true,
                // Drag and drop clones to reorder them?
                'sort_clone' => true,
                // Sub-fields
                'fields' => array(
                    array(
                        'name' => __( 'Type', 'digital_light' ),
                        'id'   => "{$prefix}protfolio_type",
                        'type' => 'select',
                        'options' => array(
                            'image' => 'Image',
                            'video' => 'Video'
                        ),
                    ),
                    array(
                        'name' => __( 'Vimeo URL', 'digital_light' ),
                        'id'   => "{$prefix}portfolio_vimeo_url",
                        'type' => 'text',
                        'hidden' => array( 'dl_protfolio_type', '!=', 'Image' ),
                        'desc' => 'Only Vimeo URLs'
                    ),
                    /*array(
                        'name' => __( 'Vimeo URL', 'digital_light' ),
                        'id'   => "{$prefix}portfolio_vimeo_url",
                        'desc' => __( 'Vimeo URL Example: https://vimeo.com/57421694', 'digital_light' ),
                        'type' => 'oembed',
                        'preview' => false
                    ),*/
                    array(
                        'name'             => __( 'Image URL', 'digital_light' ),
                        'id'               => "{$prefix}protfolio_gallery",
                        'type'             => 'image_advanced',
                        'hidden' => array( 'dl_protfolio_type', '!=', 'Video' )
                    )
                ),
            )
        )
    );

    $templates = page_templates();

    if(sizeof($templates) > 0) {

        $meta_boxes[] = array(
            // Meta box id, UNIQUE per meta box. Optional since 4.1.5
            'id' => 'page_template_box',
            // Meta box title - Will appear at the drag and drop handle bar. Required.
            'title' => __('Page Template', 'digital_light'),
            // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
            'post_types' => array('page'),
            // Where the meta box appear: normal (default), advanced, side. Optional.
            'context' => 'side',
            // Order of meta box: high (default), low. Optional.
            'priority' => 'low',
            // Auto save: true, false (default). Optional.
            'autosave' => true,
            // List of meta fields
            'fields' => array(
                // SELECT BOX
                array(
                    'id' => "{$prefix}template_select",
                    'type' => 'select',
                    // Array of 'value' => 'Label' pairs for select box
                    'options' => $templates,
                    // Select multiple values, optional. Default is false.
                    'multiple' => false,
                    'placeholder' => __('Select Template', 'digital_light'),
                )
            )
        );
    }

    $meta_boxes[] = array(
        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id' => 'page_settings_box',
        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title' => __('Page Settings', 'digital_light'),
        // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
        'post_types' => array('page'),
        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context' => 'normal',
        // Order of meta box: high (default), low. Optional.
        'priority' => 'low',
        // Auto save: true, false (default). Optional.
        'autosave' => true,
        // List of meta fields
        'fields' => array(
            // CHECKBOX
            array(
                'name' => __( 'Display Get In Touch', 'digital_light' ),
                'id'   => "{$prefix}display_get_in_touch",
                'type' => 'checkbox',
                'std'  => 0
            ),
        )
    );

    if(!empty($current_template) && $current_template == 'templates/columns') {
        $meta_boxes[] = array(
            // Meta box id, UNIQUE per meta box. Optional since 4.1.5
            'id' => 'page_columns_box',
            // Meta box title - Will appear at the drag and drop handle bar. Required.
            'title' => __('Page Columns Settings', 'digital_light'),
            // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
            'post_types' => array('page'),
            // Where the meta box appear: normal (default), advanced, side. Optional.
            'context' => 'normal',
            // Order of meta box: high (default), low. Optional.
            'priority' => 'low',
            // Auto save: true, false (default). Optional.
            'autosave' => true,
            // List of meta fields
            'fields' => array(
                // HEADING
                array(
                    'type' => 'heading',
                    'name' => __( 'Column 1', 'digital_light' ),
                    'id'   => 'column_1_heading'
                ),
                // IMAGE UPLOAD
                array(
                    'name' => __( 'Image', 'digital_light' ),
                    'id'   => "{$prefix}col_1_image",
                    'type' => 'thickbox_image',
                ),
                // TEXT
                array(
                    // Field name - Will be used as label
                    'name'  => __( 'Subheading', 'digital_light' ),
                    // Field ID, i.e. the meta key
                    'id'    => "{$prefix}col_1_subheading",
                    // Field description (optional)
                    'type'  => 'text'
                ),
                // WYSIWYG/RICH TEXT EDITOR
                array(
                    'name'    => __( 'Description', 'digital_light' ),
                    'id'      => "{$prefix}col_1_description",
                    'type'    => 'wysiwyg',
                    // Set the 'raw' parameter to TRUE to prevent data being passed through wpautop() on save
                    'raw'     => false,
                    // Editor settings, see wp_editor() function: look4wp.com/wp_editor
                    'options' => array(
                        'textarea_rows' => 7,
                        'teeny'         => true,
                        'media_buttons' => false,
                    ),
                ),
                // HEADING
                array(
                    'type' => 'heading',
                    'name' => __( 'Column 2', 'digital_light' ),
                    'id'   => 'column_2_heading'
                ),
                // IMAGE UPLOAD
                array(
                    'name' => __( 'Image', 'digital_light' ),
                    'id'   => "{$prefix}col_2_image",
                    'type' => 'thickbox_image',
                ),
                // WYSIWYG/RICH TEXT EDITOR
                array(
                    'name'    => __( 'Description', 'digital_light' ),
                    'id'      => "{$prefix}col_2_description",
                    'type'    => 'wysiwyg',
                    // Set the 'raw' parameter to TRUE to prevent data being passed through wpautop() on save
                    'raw'     => false,
                    // Editor settings, see wp_editor() function: look4wp.com/wp_editor
                    'options' => array(
                        'textarea_rows' => 7,
                        'teeny'         => true,
                        'media_buttons' => false,
                    ),
                ),
                // HEADING
                array(
                    'type' => 'heading',
                    'name' => __( 'Column 3', 'digital_light' ),
                    'id'   => 'column_3_heading'
                ),
                // IMAGE UPLOAD
                array(
                    'name' => __( 'Image', 'digital_light' ),
                    'id'   => "{$prefix}col_3_image",
                    'type' => 'thickbox_image',
                ),
                // WYSIWYG/RICH TEXT EDITOR
                array(
                    'name'    => __( 'Description', 'digital_light' ),
                    'id'      => "{$prefix}col_3_description",
                    'type'    => 'wysiwyg',
                    // Set the 'raw' parameter to TRUE to prevent data being passed through wpautop() on save
                    'raw'     => false,
                    // Editor settings, see wp_editor() function: look4wp.com/wp_editor
                    'options' => array(
                        'textarea_rows' => 7,
                        'teeny'         => true,
                        'media_buttons' => false,
                    ),
                ),
            )
        );
    }

    if(!empty($current_template) && $current_template == 'templates/contact') {
        $meta_boxes[] = array(
            // Meta box id, UNIQUE per meta box. Optional since 4.1.5
            'id' => 'page_contact_box',
            // Meta box title - Will appear at the drag and drop handle bar. Required.
            'title' => __('Page Contacts Settings', 'digital_light'),
            // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
            'post_types' => array('page'),
            // Where the meta box appear: normal (default), advanced, side. Optional.
            'context' => 'normal',
            // Order of meta box: high (default), low. Optional.
            'priority' => 'low',
            // Auto save: true, false (default). Optional.
            'autosave' => true,
            // List of meta fields
            'fields' => array(
                // TEXT
                array(
                    'name'  => __( 'Latitude', 'digital_light' ),
                    'id'    => "{$prefix}contact_lat_map",
                    'type'  => 'text'
                ),
                array(
                    'name'  => __( 'Longitude', 'digital_light' ),
                    'id'    => "{$prefix}contact_lng_map",
                    'type'  => 'text'
                ),
                array(
                    'name'  => __( 'Zoom', 'digital_light' ),
                    'id'    => "{$prefix}contact_zoom_map",
                    'type'  => 'text'
                )
            )
        );
    }

    return $meta_boxes;
}