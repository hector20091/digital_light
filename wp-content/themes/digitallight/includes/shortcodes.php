<?php

add_shortcode('clients_list','clients_list_shortcode');
function clients_list_shortcode( $atts ) {
    global $post;

    $html = '';
    $clients = get_posts(array(
        'post_type' => 'dl_clients',
        'numberposts' => -1
    ));

    if(sizeof($clients) > 0) {

        $html .= '<ul class="clients-list">';

        foreach ($clients as $post) {
            setup_postdata($post);
            $thumbnail_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
            $thumbnail_img = aq_resize($thumbnail_url,null,108,false,true,true);

            $html .= '<li><img src="' . $thumbnail_img . '"></li>';

        } wp_reset_postdata();

        $html .= '</ul>';

    } wp_reset_query();

    return $html;
}