<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://metabox.io/docs/registering-meta-boxes/
 */
add_filter( 'rwmb_meta_boxes', 'orion_2016_register_meta_boxes' );
/**digital_light
 * Register meta boxes
 *
 * Remember to change "your_prefix" to actual prefix in your project
 *
 * @param array $meta_boxes List of meta boxes
 *
 * @return array
 */
function orion_2016_register_meta_boxes( $meta_boxes )
{
    /**
     * prefix of meta keys (optional)
     * Use underscore (_) at the beginning to make keys hidden
     * Alt.: You also can make prefix empty to disable it
     */
    // Better has an underscore as last sign
    $prefix = 'orion_2016_';

    // Banner Meta Box
    $meta_boxes[] = array(
        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id'         => 'banner',
        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title'      => __( 'Title Banner', 'orion-2016' ),
        // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
        'post_types' => array( 'post', 'page', 'topfive', 'inthenews' ),
        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context'    => 'normal',
        // Order of meta box: high (default), low. Optional.
        'priority'   => 'high',
        // Auto save: true, false (default). Optional.
        'autosave'   => true,
        // List of meta fields
        'fields'     => array(
            // HEADLINE
            array(
                'name' => __( 'Banner Text', 'orion-2016' ),
                'desc' => __( 'Textarea description', 'orion-2016' ),
                'id'   => "{$prefix}banner_text",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 3,
            ),

            // SUBHEADLINE
            array(
                'name' => __( 'Subhead Text', 'orion-2016' ),
                'desc' => __( 'Textarea description', 'orion-2016' ),
                'id'   => "{$prefix}subhead_text",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 3,
            ),

            // IMAGE ADVANCED (WP 3.5+)
            array(
                'name'             => __( 'Banner Image Upload', 'orion-2016' ),
                'id'               => "{$prefix}banner_image",
                'type'             => 'image_advanced',
                'max_file_uploads' => 1,
            ),

            // IMAGE UPLOAD
            array(
                'name' => __( 'Image Upload', 'orion-2016' ),
                'id'   => "{$prefix}image",
                'type' => 'image',
            ),
            // THICKBOX IMAGE UPLOAD (WP 3.3+)
            array(
                'name' => __( 'Thickbox Image Upload', 'orion-2016' ),
                'id'   => "{$prefix}thickbox",
                'type' => 'thickbox_image',
            ),
            // PLUPLOAD IMAGE UPLOAD (WP 3.3+)
            array(
                'name'             => __( 'Plupload Image Upload', 'orion-2016' ),
                'id'               => "{$prefix}plupload",
                'type'             => 'plupload_image',
                'max_file_uploads' => 4,
            ),
            // IMAGE ADVANCED (WP 3.5+)
            array(
                'name'             => __( 'Image Advanced Upload', 'orion-2016' ),
                'id'               => "{$prefix}imgadv",
                'type'             => 'image_advanced',
                'max_file_uploads' => 4,
            ),
        ),
    );

    // Project Details Meta Box
    $meta_boxes[] = array(
        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id'         => 'details',
        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title'      => __( 'Property Details', 'orion-2016' ),
        // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
        'post_types' => array( 'featured' ),
        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context'    => 'normal',
        // Order of meta box: high (default), low. Optional.
        'priority'   => 'high',
        // Auto save: true, false (default). Optional.
        'autosave'   => true,
        // List of meta fields
        'fields'     => array(
            // TEXTAREA
            array(
                'name' => __( 'Address', 'orion-2016' ),
                'desc' => __( 'Property Address', 'orion-2016' ),
                'id'   => "{$prefix}address",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 3,
            ),

            // TEXTAREA
            array(
                'name' => __( 'Sales Price', 'orion-2016' ),
                'desc' => __( 'Closing Price', 'orion-2016' ),
                'id'   => "{$prefix}price",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 3,
            ),

            // TEXTAREA
            array(
                'name' => __( 'Property Type', 'orion-2016' ),
                'desc' => __( 'Retail/Office/Multifamily/Etc', 'orion-2016' ),
                'id'   => "{$prefix}property_type",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 3,
            ),

            // TEXTAREA
            array(
                'name' => __( 'Square Footage', 'orion-2016' ),
                'desc' => __( 'Overall Property SQ FT', 'orion-2016' ),
                'id'   => "{$prefix}square_foot",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 3,
            ),

            // TEXTAREA
            array(
                'name' => __( 'Listing Agents', 'orion-2016' ),
                'desc' => __( 'Agent(s) associated with property closing', 'orion-2016' ),
                'id'   => "{$prefix}listing_agents",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 3,
            ),
        ),
    );

    // Agent Contact Meta Box
    $meta_boxes[] = array(
        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id'         => 'contact',
        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title'      => __( 'Agent Contact', 'orion-2016' ),
        // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
        'post_types' => array( 'teammember' ),
        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context'    => 'normal',
        // Order of meta box: high (default), low. Optional.
        'priority'   => 'high',
        // Auto save: true, false (default). Optional.
        'autosave'   => true,
        // List of meta fields
        'fields'     => array(
            // TEXTAREA
            array(
                'name' => __( 'Office Phone', 'orion-2016' ),
                'desc' => __( 'Office Telephone', 'orion-2016' ),
                'id'   => "{$prefix}telephone",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 1,
            ),

            // TEXTAREA
            array(
                'name' => __( 'Cell Phone', 'orion-2016' ),
                'desc' => __( 'Cell Phone Number', 'orion-2016' ),
                'id'   => "{$prefix}cellular",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 1,
            ),

            // TEXTAREA
            array(
                'name' => __( 'Email Address', 'orion-2016' ),
                'desc' => __( 'Agent Email', 'orion-2016' ),
                'id'   => "{$prefix}email_address",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 1,
            ),

            // TEXTAREA
            array(
                'name' => __( 'AZ DRE License #', 'orion-2016' ),
                'desc' => __( 'License Number', 'orion-2016' ),
                'id'   => "{$prefix}license_number",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 1,
            ),
        ),
    );

    // Closed Property Details Meta Box
    $meta_boxes[] = array(
        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id'         => 'closing',
        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title'      => __( 'Closing Details', 'orion-2016' ),
        // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
        'post_types' => array( 'propertiesclosed' ),
        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context'    => 'normal',
        // Order of meta box: high (default), low. Optional.
        'priority'   => 'high',
        // Auto save: true, false (default). Optional.
        'autosave'   => true,
        // List of meta fields
        'fields'     => array(
            // TEXTAREA
            array(
                'name' => __( 'Property Address', 'orion-2016' ),
                'desc' => __( 'Address of the Closed Property', 'orion-2016' ),
                'id'   => "{$prefix}closed_address",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 1,
            ),

            // TEXTAREA
            array(
                'name' => __( 'Closing Price', 'orion-2016' ),
                'desc' => __( 'Price at the time of closing', 'orion-2016' ),
                'id'   => "{$prefix}closing_price",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 1,
            ),

            // TEXTAREA
            array(
                'name' => __( 'Property Type', 'orion-2016' ),
                'desc' => __( 'Retail/Office/Multifamily/Etc', 'orion-2016' ),
                'id'   => "{$prefix}closing_type",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 1,
            ),

            // TEXTAREA
            array(
                'name' => __( 'Square Footage', 'orion-2016' ),
                'desc' => __( 'Overall Property SQ FT', 'orion-2016' ),
                'id'   => "{$prefix}closing_footage",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 1,
            ),

            // TEXTAREA
            array(
                'name' => __( 'Listing Agents', 'orion-2016' ),
                'desc' => __( 'Agent(s) associated with property closing', 'orion-2016' ),
                'id'   => "{$prefix}closing_agents",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 3,
            ),

            // TEXTAREA
            array(
                'name' => __( 'Property Highlights', 'orion-2016' ),
                'desc' => __( 'Highlights of the closing', 'orion-2016' ),
                'id'   => "{$prefix}closing_highlights",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 3,
            ),
        ),
    );

    // Slider Meta Box
    $meta_boxes[] = array(
        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id'         => 'slider',
        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title'      => __( 'Slider Images', 'orion-2016' ),
        // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
        'post_types' => array( 'propertiesclosed', 'featured' ),
        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context'    => 'normal',
        // Order of meta box: high (default), low. Optional.
        'priority'   => 'high',
        // Auto save: true, false (default). Optional.
        'autosave'   => true,
        // List of meta fields
        'fields'     => array(

            // IMAGE ADVANCED (WP 3.5+)
            array(
                'name'             => __( 'Slider Images Upload', 'orion-2016' ),
                'id'               => "{$prefix}sliders_image",
                'type'             => 'image_advanced',
                'max_file_uploads' => 10,
            ),
        ),
    );

    // Project Details Meta Box
    $meta_boxes[] = array(
        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id'         => 'fourpillars',
        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title'      => __( 'Property Details', 'orion-2016' ),
        // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
        'post_types' => array( 'fourpillars' ),
        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context'    => 'normal',
        // Order of meta box: high (default), low. Optional.
        'priority'   => 'high',
        // Auto save: true, false (default). Optional.
        'autosave'   => true,
        // List of meta fields
        'fields'     => array(
            // TEXTAREA
            array(
                'name' => __( 'Banner Text', 'orion-2016' ),
                'desc' => __( 'Textarea description', 'orion-2016' ),
                'id'   => "{$prefix}banner_text",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 3,
            ),
        ),
    );

    // 1st meta box
    $meta_boxes[] = array(
        // Meta box id, UNIQUE per meta box. Optional since 4.1.5
        'id'         => 'standard',
        // Meta box title - Will appear at the drag and drop handle bar. Required.
        'title'      => __( 'Standard Text', 'orion-2016' ),
        // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
        'post_types' => array( 'post', 'page' ),
        // Where the meta box appear: normal (default), advanced, side. Optional.
        'context'    => 'normal',
        // Order of meta box: high (default), low. Optional.
        'priority'   => 'high',
        // Auto save: true, false (default). Optional.
        'autosave'   => true,
        // List of meta fields
        'fields'     => array(
            // TEXT
            array(
                // Field name - Will be used as label
                'name'  => __( 'Text', 'orion-2016' ),
                // Field ID, i.e. the meta key
                'id'    => "{$prefix}text",
                // Field description (optional)
                'desc'  => __( 'Text description', 'orion-2016' ),
                'type'  => 'text',
                // Default value (optional)
                'std'   => __( 'Default text value', 'orion-2016' ),
                // CLONES: Add to make the field cloneable (i.e. have multiple value)
                'clone' => true,
            ),
            // CHECKBOX
            array(
                'name' => __( 'Checkbox', 'orion-2016' ),
                'id'   => "{$prefix}checkbox",
                'type' => 'checkbox',
                // Value can be 0 or 1
                'std'  => 1,
            ),
            // RADIO BUTTONS
            array(
                'name'    => __( 'Radio', 'orion-2016' ),
                'id'      => "{$prefix}radio",
                'type'    => 'radio',
                // Array of 'value' => 'Label' pairs for radio options.
                // Note: the 'value' is stored in meta field, not the 'Label'
                'options' => array(
                    'value1' => __( 'Label1', 'orion-2016' ),
                    'value2' => __( 'Label2', 'orion-2016' ),
                ),
            ),
            // SELECT BOX
            array(
                'name'        => __( 'Select', 'orion-2016' ),
                'id'          => "{$prefix}select",
                'type'        => 'select',
                // Array of 'value' => 'Label' pairs for select box
                'options'     => array(
                    'value1' => __( 'Label1', 'orion-2016' ),
                    'value2' => __( 'Label2', 'orion-2016' ),
                ),
                // Select multiple values, optional. Default is false.
                'multiple'    => false,
                'std'         => 'value2',
                'placeholder' => __( 'Select an Item', 'orion-2016' ),
            ),
            // HIDDEN
            array(
                'id'   => "{$prefix}hidden",
                'type' => 'hidden',
                // Hidden field must have predefined value
                'std'  => __( 'Hidden value', 'orion-2016' ),
            ),
            // PASSWORD
            array(
                'name' => __( 'Password', 'orion-2016' ),
                'id'   => "{$prefix}password",
                'type' => 'password',
            ),
            // TEXTAREA
            array(
                'name' => __( 'Textarea', 'orion-2016' ),
                'desc' => __( 'Textarea description', 'orion-2016' ),
                'id'   => "{$prefix}textarea",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 3,
            ),

            // optional override of default jquery.validate messages
            'messages' => array(
                "{$prefix}password" => array(
                    'required'  => __( 'Password is required', 'orion-2016' ),
                    'minlength' => __( 'Password must be at least 7 characters', 'orion-2016' ),
                ),
            )
        )
    );
    // 2nd meta box
    $meta_boxes[] = array(
        'title'  => __( 'Advanced Fields', 'orion-2016' ),
        'fields' => array(
            // HEADING
            array(
                'type' => 'heading',
                'name' => __( 'Heading', 'orion-2016' ),
                'id'   => 'fake_id', // Not used but needed for plugin
                'desc' => __( 'Optional description for this heading', 'orion-2016' ),
            ),
            // SLIDER
            array(
                'name'       => __( 'Slider', 'orion-2016' ),
                'id'         => "{$prefix}slider",
                'type'       => 'slider',
                // Text labels displayed before and after value
                'prefix'     => __( '$', 'orion-2016' ),
                'suffix'     => __( ' USD', 'orion-2016' ),
                // jQuery UI slider options. See here http://api.jqueryui.com/slider/
                'js_options' => array(
                    'min'  => 10,
                    'max'  => 255,
                    'step' => 5,
                ),
            ),
            // NUMBER
            array(
                'name' => __( 'Number', 'orion-2016' ),
                'id'   => "{$prefix}number",
                'type' => 'number',
                'min'  => 0,
                'step' => 5,
            ),
            // DATE
            array(
                'name'       => __( 'Date picker', 'orion-2016' ),
                'id'         => "{$prefix}date",
                'type'       => 'date',
                // jQuery date picker options. See here http://api.jqueryui.com/datepicker
                'js_options' => array(
                    'appendText'      => __( '(yyyy-mm-dd)', 'orion-2016' ),
                    'dateFormat'      => __( 'yy-mm-dd', 'orion-2016' ),
                    'changeMonth'     => true,
                    'changeYear'      => true,
                    'showButtonPanel' => true,
                ),
            ),
            // DATETIME
            array(
                'name'       => __( 'Datetime picker', 'orion-2016' ),
                'id'         => $prefix . 'datetime',
                'type'       => 'datetime',
                // jQuery datetime picker options.
                // For date options, see here http://api.jqueryui.com/datepicker
                // For time options, see here http://trentrichardson.com/examples/timepicker/
                'js_options' => array(
                    'stepMinute'     => 15,
                    'showTimepicker' => true,
                ),
            ),
            // TIME
            array(
                'name'       => __( 'Time picker', 'orion-2016' ),
                'id'         => $prefix . 'time',
                'type'       => 'time',
                // jQuery datetime picker options.
                // For date options, see here http://api.jqueryui.com/datepicker
                // For time options, see here http://trentrichardson.com/examples/timepicker/
                'js_options' => array(
                    'stepMinute' => 5,
                    'showSecond' => true,
                    'stepSecond' => 10,
                ),
            ),
            // COLOR
            array(
                'name' => __( 'Color picker', 'orion-2016' ),
                'id'   => "{$prefix}color",
                'type' => 'color',
            ),
            // CHECKBOX LIST
            array(
                'name'    => __( 'Checkbox list', 'orion-2016' ),
                'id'      => "{$prefix}checkbox_list",
                'type'    => 'checkbox_list',
                // Options of checkboxes, in format 'value' => 'Label'
                'options' => array(
                    'value1' => __( 'Label1', 'orion-2016' ),
                    'value2' => __( 'Label2', 'orion-2016' ),
                ),
            ),
            // AUTOCOMPLETE
            array(
                'name'    => __( 'Autocomplete', 'orion-2016' ),
                'id'      => "{$prefix}autocomplete",
                'type'    => 'autocomplete',
                // Options of autocomplete, in format 'value' => 'Label'
                'options' => array(
                    'value1' => __( 'Label1', 'orion-2016' ),
                    'value2' => __( 'Label2', 'orion-2016' ),
                ),
                // Input size
                'size'    => 30,
                // Clone?
                'clone'   => false,
            ),
            // EMAIL
            array(
                'name' => __( 'Email', 'orion-2016' ),
                'id'   => "{$prefix}email",
                'desc' => __( 'Email description', 'orion-2016' ),
                'type' => 'email',
                'std'  => 'name@email.com',
            ),
            // RANGE
            array(
                'name' => __( 'Range', 'orion-2016' ),
                'id'   => "{$prefix}range",
                'desc' => __( 'Range description', 'orion-2016' ),
                'type' => 'range',
                'min'  => 0,
                'max'  => 100,
                'step' => 5,
                'std'  => 0,
            ),
            // URL
            array(
                'name' => __( 'URL', 'orion-2016' ),
                'id'   => "{$prefix}url",
                'desc' => __( 'URL description', 'orion-2016' ),
                'type' => 'url',
                'std'  => 'http://google.com',
            ),
            // OEMBED
            array(
                'name' => __( 'oEmbed', 'orion-2016' ),
                'id'   => "{$prefix}oembed",
                'desc' => __( 'oEmbed description', 'orion-2016' ),
                'type' => 'oembed',
            ),
            // SELECT ADVANCED BOX
            array(
                'name'        => __( 'Select', 'orion-2016' ),
                'id'          => "{$prefix}select_advanced",
                'type'        => 'select_advanced',
                // Array of 'value' => 'Label' pairs for select box
                'options'     => array(
                    'value1' => __( 'Label1', 'orion-2016' ),
                    'value2' => __( 'Label2', 'orion-2016' ),
                ),
                // Select multiple values, optional. Default is false.
                'multiple'    => false,
                // 'std'         => 'value2', // Default value, optional
                'placeholder' => __( 'Select an Item', 'orion-2016' ),
            ),
            // TAXONOMY
            array(
                'name'    => __( 'Taxonomy', 'orion-2016' ),
                'id'      => "{$prefix}taxonomy",
                'type'    => 'taxonomy',
                'options' => array(
                    // Taxonomy name
                    'taxonomy' => 'category',
                    // How to show taxonomy: 'checkbox_list' (default) or 'checkbox_tree', 'select_tree', select_advanced or 'select'. Optional
                    'type'     => 'checkbox_list',
                    // Additional arguments for get_terms() function. Optional
                    'args'     => array()
                ),
            ),
            // POST
            array(
                'name'        => __( 'Posts (Pages)', 'orion-2016' ),
                'id'          => "{$prefix}pages",
                'type'        => 'post',
                // Post type
                'post_type'   => 'page',
                // Field type, either 'select' or 'select_advanced' (default)
                'field_type'  => 'select_advanced',
                'placeholder' => __( 'Select an Item', 'orion-2016' ),
                // Query arguments (optional). No settings means get all published posts
                'query_args'  => array(
                    'post_status'    => 'publish',
                    'posts_per_page' => - 1,
                )
            ),
            // WYSIWYG/RICH TEXT EDITOR
            array(
                'name'    => __( 'WYSIWYG / Rich Text Editor', 'orion-2016' ),
                'id'      => "{$prefix}wysiwyg",
                'type'    => 'wysiwyg',
                // Set the 'raw' parameter to TRUE to prevent data being passed through wpautop() on save
                'raw'     => false,
                'std'     => __( 'WYSIWYG default value', 'orion-2016' ),
                // Editor settings, see wp_editor() function: look4wp.com/wp_editor
                'options' => array(
                    'textarea_rows' => 4,
                    'teeny'         => true,
                    'media_buttons' => false,
                ),
            ),
            // DIVIDER
            array(
                'type' => 'divider',
                'id'   => 'fake_divider_id', // Not used, but needed
            ),
            // FILE UPLOAD
            array(
                'name' => __( 'File Upload', 'orion-2016' ),
                'id'   => "{$prefix}file",
                'type' => 'file',
            ),
            // FILE ADVANCED (WP 3.5+)
            array(
                'name'             => __( 'File Advanced Upload', 'orion-2016' ),
                'id'               => "{$prefix}file_advanced",
                'type'             => 'file_advanced',
                'max_file_uploads' => 4,
                'mime_type'        => 'application,audio,video', // Leave blank for all file types
            ),
            // IMAGE UPLOAD
            array(
                'name' => __( 'Image Upload', 'orion-2016' ),
                'id'   => "{$prefix}image",
                'type' => 'image',
            ),
            // THICKBOX IMAGE UPLOAD (WP 3.3+)
            array(
                'name' => __( 'Thickbox Image Upload', 'orion-2016' ),
                'id'   => "{$prefix}thickbox",
                'type' => 'thickbox_image',
            ),
            // PLUPLOAD IMAGE UPLOAD (WP 3.3+)
            array(
                'name'             => __( 'Plupload Image Upload', 'orion-2016' ),
                'id'               => "{$prefix}plupload",
                'type'             => 'plupload_image',
                'max_file_uploads' => 4,
            ),
            // IMAGE ADVANCED (WP 3.5+)
            array(
                'name'             => __( 'Image Advanced Upload', 'orion-2016' ),
                'id'               => "{$prefix}imgadv",
                'type'             => 'image_advanced',
                'max_file_uploads' => 4,
            ),
            // BUTTON
            array(
                'id'   => "{$prefix}button",
                'type' => 'button',
                'name' => ' ', // Empty name will "align" the button to all field inputs
            ),
        )
    );
    return $meta_boxes;
}